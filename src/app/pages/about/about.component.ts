import { Component, OnInit } from '@angular/core'
import { ucs2 } from 'punycode'
import { UseCase } from './usecases/usecase.model'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  readonly NO_PRECOND = 'Geen'
  readonly SURVEY_CREATED = 'Er is een survey aanwezig'

  useCases: UseCase[] = [
    {
      id: this.UC(1),
      name: 'Creëren Survey',
      description: 'De gebruiker maakt een survey aan.',
      scenario: [
        'De gebruiker opent de website en ziet de voorpagina.',
        'De gebruiker klikt op "Creëren survey".',
        'De gebruiker vult een survey vraag in.',
        'De gebruiker past de survey vraag type aan indien nodig.',
        'De gebruiker voegt een nieuwe survey vraag toe als dit gewilt is en herhaalt afgelopen 2 stappen.',
        'De gebruiker publiceert de survey.',
        'De gebruiker wordt naar de resultaten pagina van de survey gestuurd.'
      ],
      actor: this.PLAIN_USER,
      precondition: this.NO_PRECOND,
      postcondition: 'Er is een survey gecreëerd.'
    },
    {
      id: this.UC(2),
      name: 'Invullen Survey',
      description: 'De gebruiker vult in een survey in.',
      scenario: [
        'De gebruiker opent de website en ziet de voorpagina.',
        'De gebruiker klikt op \'invullen\' van een survey in de lijst.',
        'De gebruiker vult de vragen in van de survey.',
        'De gebruiker levert de survey in.',
        'De gebruiker wordt naar de resultaten pagina van de survey gestuurd.'
      ],
      actor: this.PLAIN_USER,
      precondition: this.SURVEY_CREATED,
      postcondition: 'Er is een survey ingevuld.'
    },
    {
      id: this.UC(3),
      name: 'Bekijken resultaten survey',
      description: 'De gebruiker bekijkt de resultaten van een survey.',
      scenario: [
        'De gebruiker opent de website en ziet de voorpagina.',
        'De gebruiker klikt op \'bekijk resultaten\' van een survey in de lijst.',
        'De gebruiker ziet een lijst met vragen met voor elke vraag een knop \'antwoorden\' en een knop \'statistiek\' als de type vraag dit toelaat.',
        'De gebruiker klikt op een van de knoppen.',
        'De gebruiker ziet de resultaten of een grafiek.',
        'De gebruiker herhaalt dit zo nodig bij andere vragen.'
      ],
      actor: this.PLAIN_USER,
      precondition: this.SURVEY_CREATED,
      postcondition: 'De gebruiker heeft de resultaten van de survey bekeken'
    }
  ]

  UC(index: number): string {
    return 'UC-' + index;
  }

  constructor() {}

  ngOnInit() {}
}
