import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartDataset, Color } from 'chart.js';
import { Question } from 'src/assets/classes/question';
import { Survey } from 'src/assets/classes/survey';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  surveyId: string | null = null;

  survey: Survey = new Survey("No-Survey-Found Survey", -13);

  answersFormatted: Map<any, number>[] = [ 
    new Map([["I feel surprised", 2], ["I feel sad", 1],]),
    new Map([[4, 2], [3, 1]]),
    new Map([[18, 2], [16, 1]]),
    new Map([["Option 1", 0], ["Option 2", 2], ["Option 3", 1]])
  ]; // for each question a map, with each answer and amount of it

  chartDataList: ChartDataset[][] = [[]];
  chartLabels: string[][] = [];
  chartOptions = {
    responsive: true,
  };
  chartColors: any[] = [
    { // first color
      backgroundColor: 'rgba(225,10,24,0.2)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    },
    { // second color
      backgroundColor: 'rgba(225,10,24,0.2)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }];
  chartLegend = true;
  chartPlugins = [];
  chartType = 'bar';

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.surveyId = this.route.snapshot.paramMap.get('id');
    
    this.http.get<any>('http://localhost:8080/getanswers?id='+this.surveyId).subscribe((data) => {
      console.log(data);
      this.survey.title = data.title;
      this.survey.id = Number(data.id);

      for (let i = 0; i < data.questions.length; i++) {
        this.survey.addQuestion(new Question(data.questions[i].title, Number(data.questions[i].answertype)));
        this.chartDataList[i] = [{
          data: [], label: this.survey.questions[i].title, backgroundColor: ['rgba(27, 67, 50, 0.75)'], hoverBackgroundColor: ['rgba(64, 145, 108, 0.75)']
        }];
        if (Number(data.questions[i].answertype) == Question.MULTIPLE_CHOICE) {
          for (let o = 0; o < data.questions[i].options.length; o++) {
            this.survey.questions[i].setOption(o, String(data.questions[i].options[o]));
          }
        }
        
        this.answersFormatted[i] = new Map();
        this.chartLabels[i] = [];
        for (let a = 0; a < data.questions[i].answers.length; a++) {
          this.answersFormatted[i].set(data.questions[i].answers[a].answer, data.questions[i].answers[a].amount);
          this.chartDataList[i][0].data[a] = data.questions[i].answers[a].amount;
          this.chartLabels[i][a] = data.questions[i].answers[a].answer;
        }
      }
    });
  }

  ngOnInit(): void {
    this.surveyId = this.route.snapshot.paramMap.get('id');
    console.log("Answering survey ID: ", this.surveyId);
  }

}
