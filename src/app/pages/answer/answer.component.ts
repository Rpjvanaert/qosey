import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from 'src/assets/classes/question';
import { Survey } from 'src/assets/classes/survey';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  surveyId: string | null = null;

  survey: Survey = new Survey("No-Survey-Found Survey", -13);

  answers: any[] = [];

  constructor(private router: Router ,private route: ActivatedRoute, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.surveyId = this.route.snapshot.paramMap.get('id');
    console.log("Answering survey ID: ", this.surveyId);

    // get survey from server
    this.http.get<any>('http://localhost:8080/getsurvey?id='+this.surveyId).subscribe((data) => {
      console.log(data);
      this.survey.title = data.title;
      this.survey.id = Number(data.id);

      for (let i = 0; i < data.questions.length; i++) {
        this.survey.addQuestion(new Question(data.questions[i].title, Number(data.questions[i].answertype)));
        console.log(Number(data.questions[i].answertype) + " == " + Question.MULTIPLE_CHOICE);
        if (Number(data.questions[i].answertype) == Question.MULTIPLE_CHOICE) {
          for (let o = 0; o < data.questions[i].options.length; o++) {
            this.survey.questions[i].setOption(o, String(data.questions[i].options[o]));
          }
        }
      }
    });
  }

  changeOption(event: any, answerIndex: number) {
    this.answers[answerIndex] = event.target.value;
  }

  submit() {
    let any: any = null;
    let resBody = {
      id: this.survey.id,
      answers: [any]
    };

    for (let i = 0; i < this.answers.length; i++) {
      resBody.answers[i] = this.answers[i];
    }

    this.http.post<any>('http://localhost:8080/addanswer', resBody).subscribe((data) => {
      console.log(data);
      this.router.navigate(["/"]);
    })
  }
}
