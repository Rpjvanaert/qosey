import { HttpClient } from '@angular/common/http';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/assets/classes/question';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  title = "Qosey survey";

  questions: Question[] = [];

  ngOnInit(): void {
  }
  constructor(private http: HttpClient, private router: Router) {
    this.addQuestion();
  }

  addQuestion() {
    let prevQuestion = this.questions[this.questions.length - 1];
    if (prevQuestion) {
      this.questions[this.questions.length] = new Question("What is your question? " + (this.questions.length + 1), Question.TEXT)
    } else {
      this.questions[0] = new Question("What is your question? " + 1, Question.TEXT);
    }
  }

  submitSurvey() {
    let body = {
      title: this.title,
      questions: [ {
        title: "text",
        answertype: 1,
        options: ["text"]
      }]
    };

    for (let i = 0; i < this.questions.length; i++) {
      body.questions[i] = {
        title: this.questions[i].title,
        answertype: this.questions[i].answerType,
        options: ["text"]
      }

      if (this.questions[i].answerType == 0) {
        for (let o = 0; o < this.questions[0].options.length; o++) {
          body.questions[i].options[o] = this.questions[i].options[o];
        }
      }
    }

    console.log(body);
    this.http.post<any>('http://localhost:8080/addsurvey', body).subscribe((data) => {
      console.log(data);
      this.router.navigate(["../answer/" + Number(data.id)]);
    })
  }

  removeQuestion(index: number) {
    if (index >= 0 && this.questions.length > 1){
      this.questions.splice(index, 1);
    }
  }
}
