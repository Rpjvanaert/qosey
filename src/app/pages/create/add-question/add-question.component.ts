import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange } from '@angular/core';
import { Question } from 'src/assets/classes/question';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit, OnChanges {

  @Input() question: Question;
  title: string;
  answerTypeString: string;
  // options: string[];

  answerTypeStrings = Question.getAllAnswerStrings()

  //@Output() questionEmitter: EventEmitter<Question> = new EventEmitter<Question>();
  @Output() deleteSelf: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { 
    this.question = new Question("What is your question? " + 1, Question.TEXT);
    this.answerTypeString = Question.TEXT_STRING;
    this.title = this.question.title;
  }

  ngOnInit(): void {
  }

  deleteOption(index: number) {
    this.question.removeOption(index);
    // console.log(this.options);
  }

  addOption() {
    this.question.addOption();
    // this.options = this.question.options;
    console.log(this.question.options);
  }

  onQuestionChange() {
    this.question.answerType = Question.getAnswerType(this.answerTypeString);
    this.question.title = this.title;
    //this.questionEmitter.emit(this.question);
  }

  // optionChange(index: number){
  //   this.question.setOption(index, this.options[index]);
  //   // console.log(this.options);
  // }

  delete() {
    this.deleteSelf.emit(true);
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange }) {
    if (changes['question'] && changes['question'].currentValue !== changes['question'].previousValue) {
      this.title = this.question.title;
      this.answerTypeString = Question.getAnswerString(this.question.answerType);
    }
  }
}
