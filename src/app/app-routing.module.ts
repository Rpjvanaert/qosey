import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { SurveylistComponent } from './core/dashboard/surveylist/surveylist.component'
import { LayoutComponent } from './core/layout/layout.component'
import { AboutComponent } from './pages/about/about.component'
import { AnswerComponent } from './pages/answer/answer.component'
import { CreateComponent } from './pages/create/create.component'
import { ResultComponent } from './pages/result/result.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { 
        path: 'dashboard',
        component: DashboardComponent,
        children: [
          {
            path: 'surveylist',
            component: SurveylistComponent
          }
        ]
      },
      {
        path: 'create',
        component: CreateComponent,
        pathMatch: 'full'
      },
      {
        path: 'answer/:id',
        component: AnswerComponent,
        pathMatch: 'full'
      },
      {
        path: 'result/:id',
        component: ResultComponent,
        pathMatch: 'full'
      },
      { path: 'about', component: AboutComponent }
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
