import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Survey } from '../../../../assets/classes/survey';

@Component({
  selector: 'app-surveylist',
  templateUrl: './surveylist.component.html',
  styleUrls: ['./surveylist.component.css']
})
export class SurveylistComponent implements OnInit {

  surveys: Survey[] = [];

  constructor(private http: HttpClient) {
    this.http.get<any>('http://localhost:8080/getsurveys?amount=4').subscribe((data) => {
      console.log(data);

      for (let i = 0; i < data.surveys.length; i++) {
        this.surveys[i] = new Survey(data.surveys[i].title, data.surveys[i].id);
      }
    });
   }

  ngOnInit(): void {
  }
}
