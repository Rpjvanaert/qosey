import { Inject, Injectable, NgModule } from "@angular/core";
import { Question } from "./question";

@Injectable({providedIn: 'root'})
@NgModule()
export class Survey {

    private _title: string;
    private _id: number;
    private _questions: Question[];

    constructor(@Inject(String) title: string, @Inject(Number) id: number) {
        this._title = title;
        this._id = id;
        this._questions = [];
    }

    get title(): string {
        return this._title;
    }
    set title(title: string) {
        this._title = title;
    }

    set id(id: number) {
        this._id = id
    }

    get id(): number {
        return this._id;
    }

    get questions(): Question[] {
        return this._questions;
    }
    addQuestion(question: Question) {
        this._questions.push(question);
    }
    removeQuestion(index: number) {
        if (index > -1) {
            this._questions.splice(index, 1);
        }
    }

    publishAnswers(answers: string[]) {
        // Publish the answers to the server
    }
}