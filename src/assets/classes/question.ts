import { Inject, Injectable, NgModule } from "@angular/core";

@Injectable({providedIn: 'root'})
@NgModule()
export class Question {

    public static MULTIPLE_CHOICE = 0;      // A B C D
    public static MULTIPLE_CHOICE_STRING = "Multiple Choice";

    public static RATING = 1;               // 0 to 10
    public static RATING_STRING = "Rating";

    public static TEXT = 2;                 // any text 
    public static TEXT_STRING = "Text";

    public static NUMBER = 3;               // any number
    public static NUMBER_STRING = "Number";


    public static getAllAnswerStrings(): string[] {
        let answerStrings: string[] = [];
        for (let type = 0; type < 4; type++) {
            answerStrings.push(Question.getAnswerString(type));
        }
        return answerStrings;
    }

    public static getAnswerString(answerType: number): string {
        switch (answerType) {
            case 0:
                return Question.MULTIPLE_CHOICE_STRING;
            case 1:
                return Question.RATING_STRING;
            case 2:
                return Question.TEXT_STRING;
            case 3:
                return Question.NUMBER_STRING;
            default:
                return "Undefined";
        }
    }

    public static getAnswerType(answerString: string): number {
        switch (answerString) {
            case Question.MULTIPLE_CHOICE_STRING:
                return Question.MULTIPLE_CHOICE;
            case Question.RATING_STRING:
                return Question.RATING;
            case Question.TEXT_STRING:
                return Question.TEXT;
            case Question.NUMBER_STRING:
                return Question.NUMBER;
            default:
                return -1;
        }
    }

    
    private _title: string;
    private _answerType: number;
    private _options: string[];

    constructor(@Inject(String) title: string, @Inject(Number)answerType: number) {
        this._title = title;
        this._answerType = answerType;
        this._options = ["Option 1"];
    }

    get title(): string {
        return this._title;
    }
    set title(title: string) {
        this._title = title;
    }

    get answerType(): number {
        return this._answerType;
    }

    getAnswerType(): string {
        return Question.getAnswerString(this.answerType);
    }

    set answerType(answerType: number) {
        this._answerType = answerType;
    }

    get options(): string[] {
        return this._options;
    }

    set options(options: string[]) {
        this._options = options;
    }

    addOption() {
        if (this._options.length < 5) this._options[this._options.length] = "Option " + (this._options.length + 1);
    }

    getOption(index: number): string {
        return this._options[index];
    }

    removeOption(index: number) {
        if (index >= 0 && this._options.length > 1){
            this._options.splice(index, 1);
          }
    }

    resetOptions() {
        this._options = ["Option 1"];
    }

    setOption(index: number, option: string) {
        this._options[index] = option;
    }
}